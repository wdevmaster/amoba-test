<?php

namespace App\Repositories\Calendar;

use Carbon\Carbon;
use App\Models\Calendar;
use App\Models\RouteData;
use App\Services\Calendar\DateFormat;
use App\Services\Calendar\CalendarSevice;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Repository implements RepositoryInterface
{
    CONST VARS_DATE_FORMAT = ['tr', 'sd', 'ed'];
    CONST VAR_ROUTE_ID = 'r_id';

    protected $model;
    protected $with = ['routesData'];

    public function __construct() {
        $this->model = app(Calendar::class);
    }

    /**
     * Return all calendar records found.
     *
     * @param array  $request
     * @return array
     */
    public function all()
    {
        return $this->model->with()->get();
    }

    /**
     * Get the content of the register.
     *
     * @param array  $request
     * @param integer  $id
     * @return array
     */
    public function show($request, $id)
    {
        return $this->with([
            'routesData' => Calendar::whereRoutesData($request->toArray()),
            'routesData.route.services' => RouteData::whereRouteServices($request->toArray()),
            'routesData.route.reservations' => RouteData::whereRouteReservations($request->toArray()),
        ])->find($id);
    }

    /**
     * Returns the data under the calendar structure.
     *
     * @param array  $request
     * @param integer  $id
     * @return array
     */
    public function render($request, $id)
    {
        $request = $this->formatData($request);

        return (new CalendarSevice(
            $request->merge([
                'model' => $this->show($request, $id)
            ])->except(self::VAR_ROUTE_ID)
        ))->render();
    }

    /**
     * Format the date type data.
     *
     * @param array  $request
     * @return array
     */
    private function formatData($request)
    {
        return $request->merge(
            (new DateFormat($request->only(self::VARS_DATE_FORMAT)))->prepareDate()->get()
        )->except(self::VARS_DATE_FORMAT);
    }

    /**
     * Search for a specific record by id.
     *
     * @param integer  $id
     * @return $data
     */
    private function find($id)
    {
        if (!$id || null == $data = $this->model->find($id))
            throw new ModelNotFoundException("Calendar not found");

        return $data;
    }

    /**
     * Load relationships for the model.
     *
     * @param array  $with
     * @return $this
     */
    private function with($with = [])
    {
        $this->model = $this->model->with($with);
        return $this;
    }

    /**
     * Gets the count of the defined relations.
     *
     * @param array  $withCount
     * @return $this
     */
    private function withCount($withCount = [])
    {
        $this->model = $this->model->withCount($withCount);
        return $this;
    }
}
