<?php

namespace App\Repositories\Calendar;

use App\Models\Calendar;

interface RepositoryInterface
{
    public function all();

    public function show($request, $id);

    public function render($request, $id);
}
