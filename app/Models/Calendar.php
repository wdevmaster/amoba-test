<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'calendar';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at ', 'updated_at' , 'deleted_at'];

    /** Relationships */
    public function routesData()
    {
        return $this->hasMany(RouteData::class);
    }

    /** Methods Static */
    public static function whereRoutesData($values)
    {
        return function ($query) use ($values){
            $query->whereBetween('date_init', [$values['start_date'], $values['end_date']]);

            if (isset($values['r_id']))
                $query->where('route_id', $values['r_id']);

            $query->orderBy('date_init');
        };
    }
}
