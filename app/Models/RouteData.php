<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class RouteData extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'routes_data';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('withs', function (Builder $builder) {
            $builder->with(['route']);
        });
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'date_init' => 'datetime',
        'date_finish' => 'datetime',
    ];

    /** Relationships */
    public function route()
    {
        return $this->belongsTo(Route::class);
    }

    /** Methods Static */
    public static function whereRouteServices($values)
    {
        return function ($query) use ($values){
            $query->whereBetween('timestamp', [$values['start_date'], $values['end_date']]);
        };
    }

    public static function whereRouteReservations($values)
    {
        return function ($query) use ($values){
            $query->whereBetween('reservation_start', [$values['start_date'], $values['end_date']])
                  ->whereBetween('reservation_end', [$values['start_date'], $values['end_date']]);
        };
    }
}
