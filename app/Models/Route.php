<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Route extends Model
{
    const CREATED_AT = 'start_timestamp';
    const UPDATED_AT = 'end_timestamp';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'routes';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'start_timestamp ', 'end_timestamp'];

    /** Relationships */
    public function services()
    {
        return $this->hasMany(Service::class, 'external_route_id', 'external_id');
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }
}
