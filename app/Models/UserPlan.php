<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class UserPlan extends Model
{
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_plans';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created ', 'modified'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('withs', function (Builder $builder) {
            $builder->with(['user']);
        });
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'start_timestamp' => 'datetime',
        'end_timestamp' => 'datetime',
        'renewal_timestamp' => 'datetime',
        'date_max_payment' => 'datetime',
        'proxim_start_timestamp' => 'datetime',
        'proxim_end_timestamp' => 'datetime',
        'proxim_renewal_timestamp' => 'datetime',
        'date_canceled' => 'datetime',
    ];

    /** Relationships */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
