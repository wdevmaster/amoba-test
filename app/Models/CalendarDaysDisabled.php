<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CalendarDaysDisabled extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'calendar_days_disabled';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
