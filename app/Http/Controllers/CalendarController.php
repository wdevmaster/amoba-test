<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Repositories\Calendar\RepositoryInterface as CalendarRepository;

class CalendarController extends Controller
{
    /** @var CalendarRepositoryInterface */
    private $repository;

    public function __construct(CalendarRepository $repository) {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            return response()->json(
                $this->repository->all()
            );
        } catch (\Exception $e) {
            Log::debug($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Shows all the details of the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        try {
            return response()->json(
                $this->repository->show(
                    collect($request->all()), $id
                )
            );
        } catch (\Exception $e) {
            Log::debug($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Shows all the details of the specified resource formatted for the calendar.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function render(Request $request, $id)
    {
        // try {
            return response()->json(
                $this->repository->render(
                    collect($request->all()), $id
                )
            );
        // } catch (\Exception $e) {
        //     Log::debug($e->getMessage());
        //     return response()->json(['error' => $e->getMessage()], 500);
        // }
    }
}
