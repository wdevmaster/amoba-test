<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        foreach (scandir(app_path('Repositories')) as $i => $folder) {
            if ($i > 1 && !str_contains($folder, '.php'))
                $this->app->bind(
                    "App\Repositories\\$folder\RepositoryInterface",
                    "App\Repositories\\$folder\Repository"
                );

        }
    }
}

