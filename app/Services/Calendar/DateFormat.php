<?php

namespace App\Services\Calendar;

use Carbon\Carbon;
use Illuminate\Validation\ValidationException;

class DateFormat
{
    private $data;

    public function __construct($data) {
        $this->data = $this->formatDate($data);
    }
    /**
     * Prepare the filter date according to the day sent.
     */
    public function prepareDate()
    {
        $this->validateDataRequired($this->data);

        $this->validPreareType($this->data);

        $this->diffDate($this->data);

        return $this;
    }

    /**
     * get the difference between the two dates.
     */
    public function diffDate()
    {
        $this->data = $this->data->merge([
            'diff_date' => $this->data->get('start_date')->diffInDays(
                $this->data->get('end_date')
            )
        ]);
    }

    /**
     * get the data.
     */
    public function get()
    {
        return $this->data->except(['tr', 'sd', 'ed']);
    }

    /**
     * Format the data to be used in the query.
     */
    public function toDateTimeString()
    {
        return $this->data->map(function ($item, $key) {
            if ($key == 'start_date' || $key == 'end_date')
                return $item->toDateTimeString();

            return $item;
        })->except(['tr', 'sd', 'ed']);
    }

    /**
     * Validate the data required for the service.
     */
    private function validateDataRequired($data)
    {
        if (!$data->has('start_date'))
            throw ValidationException::withMessages(['sd' => 'The start day is required.']);
    }

    /**
     * Prepare the data by passing through carbon.
     */
    private function formatDate($data)
    {
        return $data->merge([
            'start_date' => $data->get('sd'),
            'end_date' => $data->get('ed') ?? $data->get('sd'),
        ])->map(function ($item, $key) {
            if ($key == 'start_date' || $key == 'end_date')
                return Carbon::parse($item);
            elseif ($key != 'sd' || $key == 'ed')
                return $item;
        });
    }

    /**
     * Verify and transform the data.
     */
    public function validPreareType($data)
    {
        if (!$data->get('tr') || $data->get('tr') == 'day')
            return $this->preareDateDay($data);

        if ($data->get('tr') == 'week')
            return $this->preareDateWeek($data);

        if ($data->get('tr') == 'month')
            return $this->preareDateMonth($data);
    }

    /**
     * Prepare the start and end date of the day.
     */
    private function preareDateDay()
    {
        return $this->data = $this->data->merge([
            'sd' => $this->data->get('start_date')->startOfDay(),
            'ed' => $this->data->get('end_date')->endOfDay(),
        ]);
    }

    /**
     * Prepare the start and end date of the week.
     */
    private function preareDateWeek()
    {
        return $this->data = $this->data->merge([
            'sd' => $this->data->get('start_date')->startOfWeek(Carbon::MONDAY),
            'ed' => $this->data->get('end_date')->endOfWeek(Carbon::SUNDAY)
        ]);
    }

    /**
     * Prepare the start and end date of the month.
     */
    private function preareDateMonth()
    {
        return $this->data = $this->data->merge([
            'sd' => $this->data->get('start_date')->startOfMonth(),
            'ed' => $this->data->get('end_date')->endOfMonth(),
        ]);
    }
}
