<?php

namespace App\Services\Calendar;

use Carbon\Carbon;

class CalendarSevice
{
    private $data;
    private $model;
    private $calender = [];

    public function __construct($data) {
        $this->data = $data->except('model');
        $this->model = $data->get('model');
    }

    /**
     * Returns the data in the form of a calendar.
     */
    public function render()
    {
        $this->createCalendarStructure();

        if (!$this->isEmptyDataRoutes())
            $this->loadCalendaWithValidRoutes();

        return collect($this->calendar);
    }

    /**
     * Validate the data to allow the operation of the service.
     */
    public function isEmptyDataRoutes()
    {
        return !$this->model || $this->model->routesData->isEmpty();
    }

    /**
     * Load the calendar array with the route data
     */
    public function loadCalendaWithValidRoutes()
    {
        $calendar = $this->calendar;

        foreach ($calendar  as $day => $value) {
            $carbonDay = Carbon::createFromFormat('Ymd', $day);
            $indxDay = strtolower($carbonDay->format('D'));

            foreach ($this->model->routesData as $row)
                if ($row->$indxDay && $carbonDay->between($row->date_init, $row->date_finish))
                    $calendar[$day][] = $this->getRowData($row, $carbonDay);
        }

        $this->calendar = $calendar;
    }

    /**
     * Get the route information
     */
    public function getRowData($row, $day = null)
    {
        $data = $row->route->only([
            "id", "invitation_code", "title", "start_timestamp", "end_timestamp"
        ]);

        if ($this->data->has('r_cap'))
            $data['capacity'] = $this->calculateCapacity($row->route, $day);

        if ($this->data->has('r_resv'))
            $data['reservations'] = $this->getDataReservationUser(
                $this->getRouteReservations($row->route->reservations, $day)
            );

        return $data;
    }

    /**
     * Obtains the user's reservation information
     */
    public function getDataReservationUser($rows)
    {
        return $rows->map(function ($row){
            return $row->userPlan->user->only(['id', 'first_name', 'last_name']);
        });
    }

    /**
     * Calculate the capacity of the route
     */
    public function calculateCapacity($route, $day)
    {
        if ($route->services->isEmpty())
            return null;

        $route_capacity = $this->getRouteServices($route->services, $day)->sum('capacity');
        $route_reservations = $this->getRouteReservations($route->reservations, $day)->count();

        return $route_capacity - $route_reservations;
    }

    /**
     * Get the services of the route for each specific day.
     */
    public function getRouteServices($services, $day)
    {
        return $services->filter(function ($service) use ($day) {
            return $day->diffInDays($service->timestamp) == 0;
        });
    }

    /**
     * Get reservations for each specific day.
     */
    public function getRouteReservations($reservations, $day)
    {
        return $reservations->filter(function ($reservation) use ($day) {
            return $day->between($reservation->reservation_start, $reservation->reservation_end);
        });
    }

    /**
     * creates the calendar structure based on the supplied date
     */
    public function createCalendarStructure()
    {
        $day = $this->data['start_date']->copy();
        $this->calendar[$day->format('Ymd')] = null;

        for ($i=0; $i < $this->data['diff_date']; $i++)
            $this->calendar[$day->addDays($i-($i-1))->format('Ymd')] = null;
    }
}
