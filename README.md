# Prueba amoba
[TOC]

## Requisitos del servidor
- PHP version >= 7.0.0
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

## Instalacion
Clonar el repositorio
```sh
git clone https://gitlab.com/wdevmaster/amoba-test.git
```
Luego aceder a la carpeta del proyecto 
```sh
cd pathTo/amoba-test
cp .env.example .env
```
### Docker
> Es necesario tener docker y docker-compose instalado para ejecutar los siguientes comandos.

#### Configuracion de entorno
```sh
docker compose up -d --build
```
Luego de que los contenedores se hayan creado correctamente pasaremos al siguiente paso.

#### Instalacion de paquete
```sh
docker compose run --rm composer install
```
#### Generar llave de aplicacion
```sh
docker compose run --rm artisan key:generate
```
#### Configuracion de base de datos
Para este apartado solo no es necesario modificar el archivo `.env` dado que ya esta preparado para trabajar bajo este entorno.

#### Ejecutar migraciones
```sh
docker compose run --rm artisan migrate:refresh --seed
```

### LAMP
#### Instalacion de paquete
```sh
composer install
```
#### Generar llave de aplicacion
```sh
php artisan key:generate
```
#### Configuracion de base de datos
En el archivo `.env` configura la base de datos en las siguientes lineas:
```sh
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```

#### Ejecutar migraciones
```sh
php artisan migrate:refresh --seed
```
#### Configuracion vHost o Dominio
> Este paso es opcional y puede ser omitido sin ningún problema.

Para que el proyecto se ejecute de manera correcta es recomendable usar un vHost si estan en un entorno local
tanto para la configuracion de vHost o el dominio debe ir apuntando a la carpeta `public/` dentro del proyecto.

### Permisos
Si a instalado el proyecto en la carpeta `www` de su servidor es necesario que le de permisos a la carpeta `storage/` y a la `bootstrap/cache` ejecutando en el siquiente comando en la bash
```sh
sudo chmod 777 -R storage/ bootstrap/cache/
```

## Postman
Hay unos archivos `.json` en la ruta `./storage/postman` que tiene toda las configuracion para realizar las pruebas mediante esta herramienta.
