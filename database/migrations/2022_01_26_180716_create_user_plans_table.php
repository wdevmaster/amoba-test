<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->bigInteger('currency_id')->unsigned()->nullable();
            $table->bigInteger('next_user_plan_id')->unsigned()->nullable();
            $table->timestamp('start_timestamp')->nullable();
            $table->timestamp('end_timestamp')->nullable();
            $table->timestamp('renewal_timestamp')->nullable();
            $table->decimal('renewal_price', 8, 2)->default(0);
            $table->integer('requires_invoice')->default(0);
            $table->integer('status')->default(0);
            $table->timestamp('created')->nullable();
            $table->timestamp('modified')->nullable();
            $table->integer('financiation')->default(0);
            $table->integer('status_financiation')->default(0);
            $table->string('language')->default('');
            $table->string('nif')->default('');
            $table->string('business_name')->default('');
            $table->decimal('pending_payment', 8, 2)->default(0);
            $table->timestamp('date_max_payment')->nullable();
            $table->timestamp('proxim_start_timestamp')->nullable();
            $table->timestamp('proxim_end_timestamp')->nullable();
            $table->timestamp('proxim_renewal_timestamp')->nullable();
            $table->decimal('proxim_renewal_price', 8, 2)->nullable();
            $table->decimal('credits_return', 8, 2)->default(0);
            $table->bigInteger('company_id')->unsigned()->nullable();
            $table->integer('cancel_employee')->default(0);
            $table->integer('force_renovation')->default(0);
            $table->timestamp('date_canceled')->nullable();
            $table->integer('amount_confirm_canceled')->nullable();
            $table->integer('credit_confirm_canceled')->nullable();
            $table->bigInteger('cost_center_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_plans');
    }
}
