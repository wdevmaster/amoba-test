<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('external_id', 10, 0)->nullable();
            $table->decimal('external_budget_id', 8, 0)->nullable();
            $table->decimal('external_route_id', 8, 0)->nullable();
            $table->bigInteger('track_id')->unsigned()->nullable();
            $table->string('name')->nullable();
            $table->string('notes')->nullable();
            $table->timestamp('timestamp')->nullable();
            $table->string('arrival_address')->nullable();
            $table->timestamp('arrival_timestamp')->nullable();
            $table->string('departure_address')->nullable();
            $table->timestamp('departure_timestamp')->nullable();
            $table->integer('capacity')->default(0);
            $table->integer('confirmed_pax_count')->default(0);
            $table->timestamp('reported_departure_timestamp')->nullable();
            $table->integer('reported_departure_kms')->nullable();
            $table->timestamp('reported_arrival_timestamp')->nullable();
            $table->integer('reported_arrival_kms')->nullable();
            $table->integer('reported_vehicle_plate_number')->nullable();
            $table->decimal('status', 10, 0)->default(0);
            $table->json('status_info')->nullable();
            $table->integer('reprocess_status')->default(0);
            $table->integer('return')->default(0);
            $table->timestamp('created')->nullable();
            $table->timestamp('modified')->nullable();
            $table->string('synchronized_downstream')->nullable();
            $table->string('synchronized_upstream')->nullable();
            $table->bigInteger('province_id')->unsigned()->nullable();
            $table->integer('sale_tickets_drivers')->default(0);
            $table->string('notes_drivers')->nullable();
            $table->text('itinerary_drivers')->nullable();
            $table->string('cost_if_externalized')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
