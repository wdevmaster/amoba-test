<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JsonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = storage_path('app/json');
        foreach (scandir($path) as $i => $file) {
            if (str_contains($file, '.json')) {
                $key = str_replace('.json', '', $file);

                var_dump($file);
                $rows = json_decode(file_get_contents("$path/$file"), true)[$key];

                foreach ($rows as $row)
                    DB::table($key)->insert($row);
            }
        }
    }
}
